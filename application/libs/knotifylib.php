<?php

class KnotifyLib {

    /**
     * We keep some constants of required parameters for each event template we are tracking. This way it's more
     * maintainable. You might consider pulling these out to other files/classes.
     *
     * When you create your Event Template in the console, use the values here for the statuses, action, and module
     * for simplicity. If you feel crazy, have at it and customize!
     */
    const SONG_CHANGE_TEMPLATE_ID = '<your_integer_here>';
    const SONGS_PAGE_MODULE_LOCATION = "App.SongsPage";
    const SONGS_PAGE_ACTION = "changed_songs";
    const SONGS_ACTION_STATUS_ADD = "add";
    const SONGS_ACTION_STATUS_EDIT = "edit";
    const SONGS_ACTION_STATUS_DELETE = "delete";

    /**
     * To obtain an access_token from the OAuth Service.
     * This uses Basic Auth to with Client API Keys to retrieve a token.
     * All other calls use the token.
     *
     * @return String access_token
     */
     private static function getKnotifyToken(){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, KNOTIFY_CLIENT_ID . ":" . KNOTIFY_CLIENT_SECRET);
        curl_setopt($curl, CURLOPT_URL, KNOTIFY_AUTH_URL);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        // Check if any error occurred
        if(curl_errno($curl))
        {
            echo 'Curl error: ' . curl_error($curl);
        } else{
            echo 'Result: ' . $result;
        }

        curl_close($curl);
        $token = json_decode($result)->access_token;
        return $token;
    }

    /**
     *
     * Generic function that will post the parameters given to the Events Service.
     * Parameters are all of the possible parameters to an Event API call.
     * See the View Event Template page at app.knotify.io for detailed info.
     *
     * @param $templateId The ID of the event template. (required)
     * @param $location Path of the location of the event in your app. Created in the console (required)
     * @param $user The end-user invoking the action (required)
     * @param $action_name The "Action Name" that's being invoked (required)
     * @param $action_status The status name for the action. One of an enumeration defined in the console (required)
     * @param $env Optional environment information. (optional)
     * @param $detail Optional additional information about the event. (optional)
     */
    public static function logEvent($templateId, $location, $user, $action_name, $action_status, $env, $detail){
        $curl = curl_init();
        if(!defined("KNOTIFY_AUTH_TOKEN")){
            define("KNOTIFY_AUTH_TOKEN", KnotifyLib::getKnotifyToken());
        }

        $body = json_encode(array('template'=>$templateId,
            'location' => $location,
            'version' => APP_VERSION,
            'end_user' => $user,
            'action' => $action_name,
            'status' => $action_status,
            'environment' => $env,
            'tech_detail' => $detail));

        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_URL, KNOTIFY_EVENTS_URL);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer " . KNOTIFY_AUTH_TOKEN,
            "Content-Type: application/json"));

        $result = curl_exec($curl);

        // Check if any error occurred
        if(!curl_errno($curl))
        {
            $info = curl_getinfo($curl);

            /**
             * These are are representative of error codes to handle from Knotify
             * But, obviously lacking in extensive retries or request handling.
             * May consider exponential-backoffs or request queuing if need be.
             */
            switch($info['http_code']){
                case 401:
                    error_log("Call was unauthorized. Reauthorizing key");
                    define("KNOTIFY_AUTH_TOKEN", KnotifyLib::getKnotifyToken());
                case 503:
                    error_log("Knotify Service Unavailable. Probably maintenance.");
                case 500:
                    error_log("Knotity service error occurred");

            }
        } else{     //If there was an Errno, something else happened when sending the request.
            error_log("A fatal error occurred trying to send the request." . curl_error($curl));
        }

    }

}