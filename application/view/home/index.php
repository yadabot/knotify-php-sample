<div class="container" xmlns="http://www.w3.org/1999/html">
    <h2>Welcome to the Knotify PHP Sample App!</h2>
    <p>This is a full application example of how to use the Knotify API with pure PHP. This sample app is designed to
        send a few different events on the <stong>Songs</stong> page: Add, Edit, and Delete. Feel free to dive into the code
        to get an initial idea of how to post Events to Knotify. You can have a look at the code without having an account
        or setting <em>anything else</em> up!</p>
    <p>If you want to setup the fully functioning demo app, you'll need an account. Everything you need to setup is in the
        <code>config.php</code> file. You'll need to put in your API keys and also some Event Template information from a
        template you can create in the Knotify Console at <a href="http://app.knotify.io">http://app.knotify.io</a></p>
    <p>Anything else? Have a look at the readme or <a href="mailto://hello@knotify.io">contact us!</a></p>
</div>
