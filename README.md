# Knotify PHP Sample Application
Hey! Thanks for checking out our code. This application is a sample full stack web application written in pure PHP designed to show you how to get started using the Knotify REST API. Authentication and Event posts using `cURL PHP` and some error handling are provided. Also provided is a [Vagrant](https://www.vagrantup.com/) Dev environment so that you can spin up the whole sample running locally on your system. Neat huh?

#The Use Case
The app has a *Songs* feature where a user can add, edit, or delete a list of songs from a list that's saved to a MySQL database. We're interested in knowing when our users perform these actions and some of the metadata about their activity.

# I Just Want To See The Important Code
Even though this is a runnable application for those looking for a real world example, we've kept our code in few spots so you can easily browse.

* `Config.php` - Here you'll find some global configuration values we added for Knotify. You'll need to update some if you run the whole app.
* `Knotifylib.php` - This is a small file containing code to authenticate using our OAuth service and `POST` generic Events. It contains some `const` variables that need to be updated if you run the whole app. This is where the RESTful magic happens.
* `Songs.php` - This is a controller file that calls `Knotifylib::logEvent` with the appropriate context information when an action occurs. **This is the part where you're *dropping events in parts of your app*.**

# Seeing it in Action
If you want to run the application, you'll need a Knotify account and [Vagrant setup](http://docs.vagrantup.com/v2/installation/index.html). If you've not used Vagrant, its easy to setup and uses Virtual Box to launch full development environments for you. Ours will run the entire application in Apache2 with MySQL on Ubuntu.

Once you've gotten these, you just need to setup a couple things in the Knotify Console and in the App.

## Knotify Setup
* Login to Knotify Console
* Click the API Access link and create a new set of API Keys.
* Click on the Templates menu item and create a new Template that corresponds with the `const` values at the top of `KnotifyLib.php`. After creating you will see the detail page describing more about the Event Request and the ID of the template. Add the Event Template ID to the `SONG_CHANGE_TEMPLATE_ID` constant in `KnotifyLib.php`.
* Add your API Keys to the constants in `config.php`.

Now when you add, edit, or delete a Song, you should see the Event Count on the Event Templates page increment for your Template. You can click the number to see details about each event.

## Run the App In Vagrant
Once you've installed Vagrant and its in your PATH:
* Clone this repo.
* `cd` into the `_vagrant` directory.
* Run `vagrant up`
* Vagrant will begin searching for the appropriate Ubuntu VM and then begin bootstrapping your environment. This may take 5 minutes or so.
* Once you see `Voila!`, That's it! Your local code you've cloned will be running on `192.168.33.44`. Go to the Songs page and play around.

### Logs
By running `vagrant ssh` you will be logged into the VM. You can head to `/var/log/apache2/` to view the error and access logs.

### Editing
The server will be running your local code. You can make any changes in your local IDE or text editor and save. The changes are immediately reflected in the running app on `192.168.33.44`.

### Stopping and Deleting
`vagrant halt` to stop. `vagrant destroy` to delete the VM and anything associated to it (not the code though).

## Using the Demo
* Navigate to the Songs page.
* Add, Edit, or Delete a Song
* Flip over to your Knotify Console and view the Events registered to your Template to see what's been sent!
* (Bonus) Setup Knotifications to send alerts when you receive certain Events!

# License

This project is licensed under the MIT License.
This means you can use and modify it for free in private or commercial projects.

Originally forked from [MINI](https://github.com/panique/mini), a tiny PHP Framework.